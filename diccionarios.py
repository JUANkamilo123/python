# Definición de diccionario vacío
diccionary = {}

# Definición de diccionario con propiedades
lp = {
    "title": "Big Ones",
    "year": 1997,
    "grammy_winner": False,
    "band": {
        "name": "Aerosmith",
        "city": "Boston"
    },
    "genres": ["rock", "blues", "ballads"],
    "awards": [
        {"id": 1, "name": "Óscars", "year": 1998},
        {"id": 2, "name": "Emmy", "year": 1998},
        {"id": 3, "name": "Grammy", "year": 2000}
    ]
}

# Acceso a una propiedad a través de la llave
print(lp["title"])
print(lp["genres"])

genres = lp["genres"]
# Recorre una propiedad de tipo List
for g in genres:
    print(g)

# Modificar la propiedad a través de un llave
lp["title"] = "Grandes Exitos"
print(lp)

# Crear una nueva clave en un diccionario,
# a través de una llave que no exista.
lp["spotify_downloads"] = 500
print(lp)

# Eliminar una propiedad
del lp["spotify_downloads"]
print(lp)

# Verificar si una llave existe en un diccionario
if "spotify_downloads" in lp:
    print("Spotify Downloads is in the dictionary")
else:
    print("Spotify Downloads is not in the dictionary")
