# Programa
from interfaz import *
from areas import *

opcion = 0
while opcion != OPCION_SALIR:
    # Pide la opcion al usuario
    opcion = menu()
    # Verifica la opcion digitada
    if opcion == OPCION_CUADRADO:
        # Pide al usuario los datos de la figura
        lado = pedir_datos_cuadrado()
        # Calcula el area de la figura
        area = area_cuadrado(lado)
        # Muestra el area
        mostrar_area(area)
    elif opcion == OPCION_TRIANGULO:
        # Pide al usuario los datos de la figura
        base, altura = pedir_datos_triangulo()
        # Calcula el area de la figura
        area = area_triangulo(base, altura)
        # Muestra el area
        mostrar_area(area)
    elif opcion == OPCION_CIRCULO:
        # Pide al usuario los datos de la figura
        radio = pedir_datos_circulo()
        # Calcula el area de la figura
        area = area_circulo(radio)
        # Muestra el area
        mostrar_area(area)

    elif opcion == OPCION_SALIR:
        # Muestra el mensaje de despedida
        mostrar_mensaje_despedida()
    else:
        # Muestra el mensaje de opcion no valida
        mostrar_mensaje_opcion_no_valida()
