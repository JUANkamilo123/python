# Libreria que se encargara de solicitar y mostrar datos al usuarios
OPCION_CUADRADO = 1
OPCION_TRIANGULO = 2
OPCION_CIRCULO = 3
OPCION_SALIR = 4


def menu():
    '''
    Muestra el menu de opciones y retorna la opcion digitada por el usuario
    '''
    print('\nCALCULADORA DE AREAS')
    print('1. Cuadrado')
    print('2. Triangulo')
    print('3. Circulo')
    print('4. Salir')
    opcion = int(input('Digite su opcion: '))
    return opcion


def pedir_datos_cuadrado():
    '''
    Pide el lado del cuadrado
    '''
    lado = float(input('Digite el lado del cuadrado: '))
    return lado


def pedir_datos_triangulo():
    '''
    Pide la base y la altura del triangulo
    '''
    base = float(input('Digite la base del triangulo: '))
    altura = float(input('Digite la altura del triangulo: '))
    return base, altura


def pedir_datos_circulo():
    '''
    Pide el radio del circulo
    '''
    radio = float(input('Digite el radio del circulo: '))
    return radio


def mostrar_area(area):
    '''
    Muesatr el area resultado
    '''
    print(f'El area es {area}')


def mostrar_mensaje_despedida():
    '''
    Muestra el mensaje de despedida
    '''
    print('Gracias por utilizar la calculadora')


def mostrar_mensaje_opcion_no_valida():
    '''
        Muestra el mensaje de opcion no valida
        '''
    print('Opción no válida')
