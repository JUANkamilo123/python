# Libreria que se va a encargar de realizar los calculos
import math


def area_cuadrado(lado):
    '''
    Calcula el area de un cuadrado dado su lado
    '''
    return lado * lado


def area_triangulo(base, altura):
    '''
    Calcula el area del triangulo dada su abse y altura
    '''
    return (base * altura) / 2


def area_circulo(radio):
    '''
    Calcula el area del circulo
    '''
    return math.pi * radio * radio
