import json


def leer_archivo():
    '''
    Lee la información de un archivo JSON y lo carga en un diccionario y lo retorna
    '''
    # Abre el archivo en modo lectura
    archivo = open("datos.json", "r")
    # Carga la información en un diccionario
    datos = json.load(archivo)
    # Cierra el archivo
    archivo.close()
    # Retorna el diccionario
    return datos


def escribir_archivo(datos):
    '''
    Escribe un diccionario en un archivo
    '''
    # Abre el archivo en modo escritura
    archivo = open("datos.json", "w")
    # Guarda el diccionario en el archivo
    json.dump(datos, archivo)
    # Cierra el archivo
    archivo.close()


# Lee los datos del archivo
datos = leer_archivo()

# Pide el nuevo nombre al usuario
nuevo_nombre = input("Digite el nuevo nombre: ")

# Modifica el diccionario con el nombre digitado
datos["nombres"] = nuevo_nombre

# Escribe el diccionario en el archivo
escribir_archivo(datos)
