import json

# Abre el archivo en modo lectura (r = read)
# Si el archivo no existe, genera un error
archivo = open("datos.json", "r")

# Carga el contenido del archivo en un diccionario
datos = json.load(archivo)

# Cierra el archivo
archivo.close()

print(datos)
print(datos["apellidos"])
