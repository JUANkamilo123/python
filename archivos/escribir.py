# Importar libreria
import json

# Definición de datos
estudiante = {
    "nombres": "James",
    "apellidos": "Bond",
    "edad": 45,
    "matriculado": True
}

# Abre el archivo en modo escritura (w)
# Si el archivo no existe lo crea,
# Si el archivo existe lo sobreescribe
archivo = open("datos.json", "w")

# Escribe los datos en el archivo
json.dump(estudiante, archivo)

# Cerrar el archivo
archivo.close()
